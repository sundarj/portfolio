import os
import pathlib

import markdown

def main():
  for post in os.listdir("portfolio/posts"):
    stem = pathlib.Path(post).stem
    with open(f"portfolio/posts/{post}", encoding="utf-8") as inputf:
      with open(f"portfolio/static/posts/{stem}.html",
                "w",
                encoding="utf-8") as outputf:
        outputf.write(f"""<!DOCTYPE html>
<html lang="en">
<head>
  <title>{stem.title()}</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://unpkg.com/sanitize.css">
  <link rel="stylesheet" href="/index.css" />
</head>
<body>
  <main>
    <section class="post">
      <div class="container">
        {markdown.markdown(inputf.read())}
      </div>
    </section>
  </main>
</body>
</html>""")

if __name__ == '__main__':
  main()