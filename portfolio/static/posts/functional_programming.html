<!DOCTYPE html>
<html lang="en">
<head>
  <title>Functional_Programming</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://unpkg.com/sanitize.css">
  <link rel="stylesheet" href="/index.css" />
</head>
<body>
  <main>
    <section class="post">
      <div class="container">
        <h1>An introduction to functional programming</h1>
<p>Functional programming is a very ambiguous buzzword. But it actually makes a great
deal of sense once you understand it. I love functional programming and prefer
it to object-oriented and logic programming, so in this post I will try to make
it clearer.</p>
<h2>What is functional programming?</h2>
<p>Functional programming is a style of programming that aims to make programming
more suited to the human programmers rather than the machine. It prioritizes
programmer time over machine time, which is not to say that functional programs
have to be inefficient, but they are generally less efficient than imperative
programs. It is a declarative style of programming, which means you say more
about what should be done rather than how it should be done. Functional
programming emphasizes the ruthless elimination of unnecessary cognitive load.
It does this through a combination of several key ideas:</p>
<ul>
<li>Immutable (constant and unchanging) data instead of mutable (changeable and
transient) objects</li>
<li>Building the bulk of your program out of first-class strictly input-to-output
functions (first-class meaning that you are able to assign functions to
variables, store them in data structures, and pass them around as arguments
etc.) instead of methods or procedures.</li>
<li>Distinguishing between code that performs an action and code that simply makes
decisions or transforms input data to output data. We isolate and minimise the
action-performing parts of our code, putting it all in one place (though of
course not all in one function).</li>
</ul>
<h2>Why would you want to do it?</h2>
<p>These ideas are all intended to make programs easier to understand, maintain,
refactor and test. They enable you to reason locally, rather than globally:</p>
<ul>
<li>Immutable data means you never need to keep track of what time
you are looking at a piece of data or worry about another piece of code changing
it out from under you. It is a much saner foundation for reasoning. It is also
maximally thread-safe.</li>
<li>
<p>Strictly input-to-output functions are much simpler and more predictable,
because they always return the same output for the same input and are not
involved with time. You only need to think about the function's input and its
body, not about the code it is interacting with. For example, this is a
strictly input-to-output function:</p>
<pre><code>function inc (x) {
  return x + 1;
}
</code></pre>
<p>while this is not:</p>
<pre><code>function mutatingInc () {
  y++;
}
</code></pre>
<p>The first one is easier to understand because its behaviour doesn't change
across time - it only depends on the arguments you pass and the body. The second
one depends on the state of the variable <code>y</code>, so it matters when you call it and
what other code could affect <code>y</code> too.</p>
</li>
<li>
<p>First-class functions give programmers the ability to manipulate functions
just like they do data: there's less of a distinction between them that you must
keep in mind. There isn't one rule for functions and another rule for data
structures - for most purposes, functions <strong>are</strong> data. For example, storing
functions in a data structure:</p>
<pre><code>const inc = (x) =&gt; x + 1;
const dec = (x) =&gt; x - 1;
const toString = (x) =&gt; String(x);
const funs = [inc, dec, toString];

for (let fun of funs) {
  console.log(fun(42));
}
</code></pre>
<p>or passing them as arguments:</p>
<pre><code>[1, 2, 3].map(inc);
</code></pre>
</li>
<li>
<p>Actions are, of course, why we run programs most of the time and functional
programs are no different. We don't totally eliminate actions (or effects) in
functional programs - we still have to store things on disk and send network
traffic - but we recognise that code that effects change in the external
world or other parts of the program is much harder to understand, so we try to
minimize it. Using as few effects as possible and all in the same place. Code
that performs an action (or has an effect) necessarily requires you to keep in
mind the state of the external world and the time the code is being run, which
is why functional programs aim to minimize and isolate it. For example:</p>
<pre><code>function parse (s) {
  return JSON.parse(s);
}

return stringify (o) {
  return JSON.stringify(o, null, 2);
}

function main () {
  const input = fs.readFileSync("input.json");
  const output = stringify(parse(input));
  console.log(output);
}
</code></pre>
<p>In this case, <code>main</code> is the only part of the program which performs actions by
interacting with the external world. <code>parse</code> and <code>stringify</code> are both strictly
input-to-output.</p>
</li>
</ul>
<h2>References</h2>
<ol>
<li><a href="https://ericnormand.me/article/procedural-paradox">Programming Paradigms and the Procedural Paradox</a>.</li>
<li><a href="https://ericnormand.me/article/composable-parts">Composable parts</a>.</li>
<li><a href="https://ericnormand.me/article/reasoning-about-code">Reasoning about code</a>.</li>
<li><a href="https://ericnormand.me/article/global-mutable-state">Global mutable state</a>.</li>
<li><a href="https://ericnormand.me/article/imperative-mindset">How to Switch from the Imperative Mindset</a>.</li>
<li><a href="https://www.yegor256.com/2014/06/09/objects-should-be-immutable.html">Objects Should Be Immutable</a>.</li>
<li><a href="https://blog.ploeh.dk/2016/09/26/decoupling-decisions-from-effects/">Decoupling decisions from effects</a>.</li>
<li><a href="https://blog.ploeh.dk/2016/03/18/functional-architecture-is-ports-and-adapters/">Functional architecture is Ports and Adapters</a>.</li>
<li><a href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a>.</li>
<li><a href="http://www.youtube.com/watch?v=f84n5oFoZBc">Hammock Driven Development</a>.</li>
<li><a href="http://www.infoq.com/presentations/Design-Composition-Performance">Design, Composition and Performance</a>.</li>
<li><a href="https://www.youtube.com/watch?v=2V1FtfBDsLU">Effective Programs</a>.</li>
<li><a href="https://github.com/conal/talk-2014-lambdajam-denotational-design">Denotational Design: from meanings to programs</a>.</li>
<li><a href="https://www.youtube.com/watch?v=FQnBOqtwuLE">A Theory of Functional Programming</a>.</li>
</ol>
      </div>
    </section>
  </main>
</body>
</html>