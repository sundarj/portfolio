# Becoming mentally healthy

# References
1. Learned Optimism - Martin Seligman, Ph.D..
2. Flourish - Martin Seligman, Ph.D..
2. Why Has Nobody Told Me This Before? - Dr. Julie Smith.
3. [Mental health](https://www.who.int/news-room/fact-sheets/detail/mental-health-strengthening-our-response).
4. [Mental health](https://www.nhs.uk/mental-health/).
5. [5 steps to mental wellbeing](https://www.nhs.uk/mental-health/self-help/guides-tools-and-activities/five-steps-to-mental-wellbeing/).
6. [Top tips to improve mental wellbeing](https://www.nhs.uk/every-mind-matters/mental-wellbeing-tips/top-tips-to-improve-your-mental-wellbeing/).
7. [How to improve your mental wellbeing](https://www.mind.org.uk/information-support/tips-for-everyday-living/wellbeing/wellbeing/).
8. [How to Improve Mental Health](https://medlineplus.gov/howtoimprovementalhealth.html).