# Books, ranked

## Excellent
- The Bible
- The Design of Everyday Things - Donald A. Norman
- Christian Theology: An Introduction - Alister E. McGrath
- Learned Optimism - Martin Seligman
- Studies in the Sermon on the Mount - D. Martyn Lloyd-Jones

## Great
- The Resurrection of Jesus: A New Historiographical Approach - Michael R. Licona
- The Case for Christ - Lee Strobel
- The Cross - Martyn Lloyd-Jones
- Kant: A Very Short Introduction - Roger Scruton
- How to Hear God - Pete Greig

## Good
- Plato: A Very Short Introduction - Julia Annas
- Confucianism: A Very Short Introduction - Daniel K. Gardner
- Judaism: A Very Short Introduction - Norman Solomon
- Islam: A Very Short Introduction - Malise Ruthven
- Scripture and the Authority of God - Tom Wright
- Mere Christianity - C.S. Lewis
- Evidence That Demands A Verdict - Josh McDowell & Sean McDowell
- Why Has Nobody Told Me This Before? - Julie Smith
- Florence Nightingale: Letters and Reflections - Rosemary Hartill
- Letters of Note - Shaun Usher
- Quirkology - Richard Wiseman
- Sex, Drugs & Rock n Roll: The Science of Hedonism and the Hedonism of Science - Zoe Cormier
- How the Mind Works - Stephen Pinker
- Shantaram - Gregory David Roberts
- Problem Frames - Michael Jackson
- Reimagining Britain - Justin Welby

## Mediocre
- Mathematics: A Very Short Introduction - Timothy Gowers
- Computer Science: A Very Short Introduction - Subrata Dasgupta
- Thought: A Very Short Introduction - Tim Bayne

## Bad
- Logic: A Very Short Introduction - Graham Priest
- The Qur'an