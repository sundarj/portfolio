# Why believe Christianity?
Christianity is centred on the person of Jesus of Nazareth (called Christ,
which means 'anointed one'). Christians believe him to be the central figure of
human history, the Creator God of the universe come as a human being.
But is this belief rational? In this article, I hope to clearly set forth the
facts, which a thinking person may evaluate and come to their own conclusions.

## Table of Contents
- Why believe in God?
- Why believe in the resurrection of Jesus Christ?
- Why believe in Jesus Christ?
- Why believe in the Bible?
- References

## Why believe in God?

We can infer God's existence from nature - firstly, there is the fact that the
universe exists rather than nothing whatsoever. So we have to ask: why does it
exist? How did it come into being in the first place? Has it always existed or
not?

As [8] points out, there are four possible answers to this question:

1. Reality is an illusion.
2. Reality is/was self-created.
3. Reality is self-existent (eternal).
4. Reality was created (or caused) by something that is self-existent.

As for reality being an illusion: an illusion is 'something that is not
really what it seems to be' [16] whereas reality is 'the state of things as
they are, rather than as they are imagined to be' [17]. In order for something
to be an illusion, you need to have a reality to compare it to. So saying
'reality is an illusion' is a contradiction. Furthermore, to even doubt reality
requires that you have a reliable mind with which to doubt. You can't doubt your
existence without assuming your existence. Hence Descarte's insight that 'I am
thinking, therefore I am'. So reality can't be an illusion.

Reality being self-created: how can something be prior to itself? If reality
created itself, then it must have existed prior to itself, which is absurd. Something
cannot come from nothing. So reality can't be self-created.

Therefore, either reality is self-existent or it was created by something that
is self-existent. So which is it? Well, to date science has concluded that the
universe began with the Big Bang. And whatever has a beginning is obviously not
self-existent. One argument, called the Kalam cosmological argument, goes like
this [12]:

1. Whatever begins to exist has a cause.
2. The universe began to exist.
3. Therefore, the universe has a cause, which we can call God.

Anether argument goes like this:

1. If God does not exist, nothing at all would exist.
2. The universe exists.
3. Therefore, God exists.

The contingency argument goes like this [9, 13]:

1. Everything that exists has an explanation for its existence (either in the
necessity of its own existence or in an external cause).
2. If the universe has an explanation for its existence, that explanation is
God.
3. The universe exists.
4. Therefore, the universe has an explanation for its existence.
5. Therefore, the explanation of the universe's existence is God.

So reality was created by something that is self-existent. It has a Creator.
What can we know of this Creator? [8]

- He must be supernatural in nature (He created nature).
- He must be timeless and changeless (He created time).
- He must be omnipresent (He created space and is not limited by it).
- He must be immaterial (He created the material world).
- He must be personal (the impersonal cannot create personality).
- He must be intelligent (supremely). Only a cognitive being could create the
laws of nature and create us cognitive beings.
- He must be moral as He created us moral beings.
- He must be caring as He gave us morals.
- He must be powerful (exceedingly).

This matches up with the portrayal of God in the Bible, though we aren't addressing the
reliability of the Bible just yet. There are two further arguments for the
existence of God that I will cover here:

The moral argument goes like this [15]:

1. If God does not exist, objective moral values and duties do not exist.
2. Objective moral values and duties do exist.
3. Therefore, God exists.

Does this make any sense? Well, if there is no God, and our evolution was
unintended, so we're merely products of blind chance and survival of the
fittest, then what objective basis do we have for morality? There is no
objective moral standard. Our moral sense is a contingent product of our
species' evolution, the environment we're brought up in, and our circumstances.
It's not rooted in any unchanging standard, like God's nature. The universe
doesn't care. When we die, we die and eventually the universe will die too.
Yet, across cultures and time periods there do seem to be objective moral
values and duties, such as the prohibition of murder and rape. Most of us make
the distinction between right and wrong, even in the absence of an ultimate
authority to enforce it, even if you believe we're just sophisticated animals
(and animals have no moral obligations). We have never valued selfishness or
foolishness or callousness. Therefore, there must be some unchanging standard
of morality that we look to. So there must be a God.

Finally, there is the teleological argument, or the argument from design:

1. The universe appears designed.
2. If it was designed, there was a designer.
3. Therefore, a designer (God) exists.

The universe is equisitely fine-tuned for the existence of intelligent life:
from the precise setting of universal constants to the location of the Earth in
the habitable zone to the number of spatial and time dimensions [4]. Then there
is the apparently objective beauty of nature and the laws of nature, as well as
the 'unreasonable' effectiveness of mathematics [18]. These imply a Creator.

## Why believe in the resurrection of Jesus Christ?

The gospels (four biographies of Jesus written in in the decades following his
death) are early, well-written, and are the best attested documents of ancient
literature - meaning that we have many more copies written much sooner than
contemporary works, which, using the science of textual criticism, gives
scholars of antiquity confidence that what we have today largely resembles what
was originally written [19]. We have reason to believe that the gospels were
intended as accurate history, even if they don't meet that bar:

- The gospel of Matthew starts his biography with a genealogy - a description of
Jesus' father's family line - thereby rooting his biography in history.
- The gospel of Mark starts with 'the beginning of the gospel of Jesus Christ,
the Son of God.' [1, p. 1893]. Gospel means 'good news', in the original Greek
it's euangelion. It was a word used for true events like the crowning of a new
king or the victory of the army in battle [20].
- The gospel of Luke starts with a clear claim to be writing history:
'Inasmuch as many have undertaken to compile a narrative of the things that have
been accomplished among us, just as those who from the beginning were
eyewitnesses and ministers of the word have delivered them to us, it seemed good
to me also, having followed all things closely for some time past, to write an
orderly account for you, most excellent Theophilus, that you may have certainty
concerning the things you have been taught.' [1, p. 1942].
- The gospel of John has this as its explicit purpose: 'Now Jesus did many other
signs in the presence of the disciples, which are not written in this book, but
these are written so that you may believe that Jesus is the Christ, the Son of
God and that by believing you may have life in his name.' [1, p. 2071]. He
therefore was writing things he claimed happened, with the aim of fostering
belief in someone he claimed was worth believing. It wouldn't be worth
believing if it was false, I imagine. He further adds: 'This is the disciple who
is bearing witness about these things, and who has written these things, and we
know that his testimony is true' [1, p. 2072].

So they probably intended to write history. Furthermore, as Peter Williams
argues [3], they show deep knowledge of Palestinian geography, Palestinian
Jewish naming conventions, and Palestinian Jewish language, customs, and
politics, indicating they either personally knew the things they were writing
about or were accurately recording the accounts of people who did. Matthew 14
has the characters disambiguate the common name of John, but the narrator
simply says John. The writer either wrote down just what people had said or had
them speak as if they were in a Palestinian context. Either way, he had
detailed cultural knowledge of the situation. Ditto Matthew 21, 26, Mark 1, 10, 14,
16, Luke 4, 8, 17, 24, and John 1, 6, 18, and 19.

Names are hard to remember, so the fact the names match how frequent those
names were in Palestine strongly implies that they were reliably writing down
what people were actually called and not making names up. Since they're hard to
remember, the authentic patterns of names implies high quality testimony. If
they have correctly remembered the less memorable details, then they should
have had no difficulty remembering the more memorable outline of events.

The gospels contain some of the world's most brilliant and exacting ethical
teaching. While it's possible they were written by scoundrels or fools, it
doesn't seem more likely than the view that the high standard comes from a
person or persons committed to strictly honest, ethical living.

All that being said, we don't have to believe them entirely to come to the conclusion
that Jesus was raised from the dead. All the gospels need to establish is some
minimal facts:

1. Jesus was crucified. Non-Christian sources Tacitus (in Annals) [3, 5],
Josephus (in Antiquities of the Jews) [5], and Mara bar Serapion [5] also speak
of his execution. 
2. His tomb was discovered empty by women followers of Jesus (the fact they
were women is an embarrassing detail since the testimony of women was not
regarded as reliable at the time).
3. His disciples had experiences that they interpreted as of Jesus alive.
4. His disciples sincerely believed he had been raised from the dead, and proclaimed it.

Given these facts, which even skeptical New Testament scholars would permit,
what is the best explanation? One explanation is that these disciples were right,
God did raise him from the dead. Another is that the disciples had group hallucinations,
which psychiatrists say isn't possible since hallucinations are subjective
experiences that no one else can see, so how could they be shared? Another is
that his body was stolen, which explains the empty tomb but not the sincere
belief that he had been raised. Another explanation is that Jesus didn't really
die on the cross, which contradicts modern medical analyses of the gospel data [2]
and the fact that the Romans were pretty good at killing people. It also doesn't
explain how he moved the large stone that was placed against his tomb, or why
the disciples would have proclaimed that this heavily wounded man was, after
several days without medical attention, miraculously raised from the dead in
glory and power and a transformed body.

The New Testament contains further literature, by Paul and Peter for example, that
attests to the reality of the resurrection. 1 Corinthians 15 is a very early
account, which shows that the resurrection tradition started early on, much too
soon for legend to spring up.

## Why believe in Jesus Christ?

According to the gospels, he did some astonishing things: healing the sick and
injured instantaneously (Matthew chapters 4 and 8; Mark 1-2; Luke 4-5; John
4-5), raising people from the dead (Matthew 9; Mark 5; Luke 7-8; John 11), calming
the wind and waves during a storm (Matthew 8; Luke 8), and feeding thousands on a small
amount of food (Matthew 14-15; Mark 6 and 8; Luke 9; John 6).

He gave transcendent and inspiring teaching, such as the Sermon on the Mount
(Matthew 5-7, Luke chapter 6) and the parables of the Prodigal Son (Luke 15) and
the Good Samaritan (Luke 10). He claimed to be one with God (John 10). He claimed
to be the only way to God, the truth, and the life (John 14). He claimed that
knowing him was knowing God, and seeing him was seeing God (John 14). He claimed
that God would love you if you loved and obeyed him (John 14). He claimed to have
authority to forgive sins (injustices; crimes against God; Matthew 9; Mark 2;
Luke 5).

And God authenticated all this by raising him from the dead to a transformed
body, never to die again.

## Why believe in the Bible?

If you believe in Jesus Christ, you should believe in the Old Testament (the
first section of the Bible), because he did [24, 25, 26, 27]. He taught from it,
grounding his message in it:

### Matthew
- Matthew 5 has Jesus saying that he did not come to abolish but fulfil the
Jewish scriptures (the Law and the Prophets are divisions of the Jewish
scriptures). He also indicated that the Law would not pass away even though
heaven and earth would.
- Matthew 9 has Jesus argue from the Old Testament book of Hosea.
- Matthew 10 has Jesus quote the Old Testament book of Micah, in describing his
purpose.
- Matthew 11 has Jesus describe John the Baptist (a prophet who preceded Jesus)
using the words of Malachi (one of the Old Testament prophets).
- Matthew 12 has Jesus reason from the portrayal of David (an Old Testament king)
in the Jewish scriptures, and the Law. He also repeats the quote from Hosea.
Finally, he references the Old Testament book of Jonah to explain what sign he
will give.
- Matthew 13 has Jesus use the words of Old Testament prophet Isaiah to describe
his reason for talking in parables.
- Matthew 15 has Jesus use the words of Isaiah to criticize the Pharisees
(Jewish religious leaders).
- Matthew 19 has Jesus quote the Old Testament book of Genesis, saying that
what Genesis says, God said. He believed Genesis to be the word of God, even
down to the commentary that isn't stated to be God speaking. He also states that
to have life, you must keep the commandments given in the Old Testament books of
Leviticus and Deuteronomy.
- Matthew 21 has Jesus quote two of the Old Testament Psalms (Psalm 8 and Psalm
118). He refers to Psalm 118 as 'in the Scriptures' (γραφαῖς lit.
'the writings').
- Matthew 22 has Jesus say that God said what was attributed to him in the Old
Testament book of Exodus. He also says that one of the Psalms of David was
written 'in the Spirit' i.e. given by God.

### Mark
- Mark 4 has Jesus teaching in a synagogue, and being compared to the teachers
of the law, who were teachers of the Jewish scriptures.
- Mark 7 has Jesus affirming that Isaiah (one of the Old Testament prophetic
books) was prophecy. It also shows him describing Moses' writings (traditionally,
Moses is held to have written the first 5 books of the Old Testament) as the
word of God.
- Mark 12 has Jesus refer to a quote from Psalm 118 as 'this Scripture'
[1, p. 1920] (γραφὴν [28] lit. 'writing'). He also refers to 'the Scriptures'
together with 'the power of God' [1, p. 1921]. Furthermore, he affirms that God
really spoke to Moses as seen in the 'book of Moses' [1, p. 1921] (the Old
Testament's Exodus). Finally, he says that one of David's Psalms (Psalm 110) is
spoken 'in the Holy Spirit' [1, p. 1921] (τῷ Πνεύματι τῷ Ἁγίῳ [28]).
- Mark 13 has Jesus mention an 'abomination of desolation' [1, p. 1923] which is
a reference to the Old Testament book of Daniel.
- As seen in Mark 14, Jesus celebrated the Jewish festival of Passover, which
is based on the Old Testament book of Exodus. He also claimed to be the Christ,
foretold by the Old Testament prophets.


### Luke
- Luke 4 has Jesus respond to temptation by quoting words of the Old Testament
book of Deuteronomy and Psalm 91. He also reads from the Old Testament book of
Isaiah, saying 'this Scripture' [1, p. 1957] has been fulfilled.
- Luke 7 has Jesus say that John the Baptist is as fulfillment of the Old
Testament book of Malachi.
- Luke 11 has Jesus affirm that all the prophets from Abel (in the Old
Testament book of Genesis) to Zechariah (in the Old Testament book of Zechariah)
were killed. Thus he is affirming the full Hebrew canon, which makes up our Old
Testament in a different order.
- Luke 16 has Jesus say that 'it is easier for heaven and earth to pass away
than for one dot of the Law to become void' [1, p. 1991] (the Law is one of the
sections of the Hebrew canon, which we find in our Old Testament). It also has
Jesus tell a parable stating that Moses (the Law) and the Prophets in the Old
Testament should be believed.
- Luke 20 has Jesus teach from one of the Psalms (Psalm 118) in the Old
Testament. It also has him teach from another one of the Psalms (Psalm 110).
- Luke 22 has Jesus celebrate the Jewish festival of Passover, which is based on
the Old Testament book of Exodus. It also has Jesus say that 'this Scripture'
[1, p. 2007] must be fulfilled in him - referring to the Old Testament book of
Isaiah.
- Luke 24 has Jesus say that everything written in the prophets about him (the
Christ) must be fulfilled, and he interprets 'the Scriptures' (γραφαῖς [28]) to
them. He also says that 'everything written about me in the [Old Testament] Law
of Moses, and the Prophets, and the Psalms must be fulfilled' [1, p. 2014].

### John
- John ...

Jesus also refers to himself often in the gospels as the Son of Man, which could be a 
reference to the Old Testament book of Daniel.

I have already given some of the reasons to believe the gospels, above. I
should add the book of Acts to this, because it was written by the same author
as Luke. That leaves the New Testament epistles (formal letters) and the book
of Revelation.

According to Acts 9, 22 and 26, Paul was commissioned by Jesus
to be an apostle and teacher. He immediately turned from zealously opposing
Christianity to being one of its most fervent proponents. So there is reason to
believe his epistles, especially since they reveal a person who had a real love
for God and others. Also see [29]. His epistles are considered to be [30, 31]: 
Romans, 1 and 2 Corinthians, Galatians, Philippians, 1 Thessalonians, and
Philemon. There are six additional letters that are disputed: Ephesians, Titus,
Colossians, 2 Thessalonians, and 1 and 2 Timothy.

1 and 2 Peter claim to be from Peter, one of Jesus' original twelve apostles.
The authorship of 1, 2, and 3 John is disputed, but they are traditionally
held to be from John, one of the twelve apostles [32]. The author of Hebrews
was traditionally held to be St. Paul, but is now considered to be by someone
else [33]. Hebrews is formally anonymous [33]. James is attributed to a James,
whose identity is disputed [34]. The letter of Jude claims to be written by a
brother of James, but the identity of the author is uncertain [35]. Finally,
Revelation claims to be by a John, whose identity is unclear [36].

I will finish by setting out some of the arguments that the Bible is the word
of God [22, 4]. One is that the Bible, despite being composed of 66 individual
books, written on three contintents, in three different languages, over a period
of approximately 1500 years, by more than 40 authors who came from many walks
of life, it tells a remarkably coherent and consistent story. There are minor
contradictions here and there, some probably intentional and some probably not,
but the core of the message is the same. It also contains much fulfilled predictive
prophecy concerning the life, death, and resurrection of Jesus Christ.
Furthermore it has a unique beauty and authority - it has had a profound
influence on Western civilization. From the creation of hospitals [37] and
universities [38], to mass literacy and education [39, 40], to the separation of political
powers [41], to the birth of modern science [42], to the high value accorded to human
life and the individual [43].

## References
1. Crossway (2008) _ESV Study Bible_. Illinois: Crossway.
2. Strobel, L. (2016) _The Case for Christ_. Michigan: Zondervan.
3. Williams, P. J. (2018) _Can We Trust the Gospels?_. Illinois: Crossway.
4. McDowell, J. and McDowell, S. (2017) _Evidence That Demands A Verdict_. Milton Keynes: Authentic Media Ltd.
5. Licona, M. R. (2010) _The Resurrection of Jesus: A New Historiographical Approach_. Illinois: InterVarsity Press.
6. Lewis, C. S. (2012) _Mere Christianity_. London: William Collins.
7. Got Questions Ministries (no date - a). _Does God exist?_. Available at: https://www.gotquestions.org/Does-God-exist.html (Accessed: 5 December 2023).
8. Got Questions Ministries (no date - b). _Is there a conclusive argument for the existence of God?_. Available at: https://www.gotquestions.org/argument-existence-God.html (Accessed: 5 December 2023).
9. CrossExamined.org (2017). _The Contingency Argument for God's Existence_. Available at: https://crossexamined.org/contingency-argument-gods-existence/ (Accessed: 5 December 2023).
10. Craig, W. L (no date). _Does God Exist?_. Available at: https://www.reasonablefaith.org/writings/popular-writings/existence-nature-of-god/does-god-exist (Accessed: 5 December 2023).
11. Craig, W. L. (no date) _Can We Be Good Without God?_. Available at: https://www.reasonablefaith.org/writings/popular-writings/existence-nature-of-god/can-we-be-good-without-god (Accessed: 5 December 2023).
12. drcraigvideos (2013). _The Kalam Cosmological Argument - Part 1: Scientific_. Available at: https://www.youtube.com/watch?v=6CulBuMCLg0 (Accessed: 5 December 2023).
13. drcraigvideos (2015a). _Leibniz’ Contingency Argument_. Available at: https://www.youtube.com/watch?v=FPCzEP0oD7I (Accessed: 5 December 2023).
14. drcraigvideos (2014). _Why is the Universe Contingent?_. Available at: https://www.youtube.com/watch?v=Wa_YhzylL-w (Accessed: 5 December 2023).
15. drcraigvideos (2015b). _The Moral Argument_. Available at: https://www.youtube.com/watch?v=OxiAikEk2vU (Accessed: 5 December 2023).
16. Cambridge University Press & Assessment (no date - a). _illusion_. Available at: https://dictionary.cambridge.org/dictionary/english/illusion (Accessed: 6 December 2023).
17. Cambridge University Press & Assessment (no date - b). _reality_. Available at: https://dictionary.cambridge.org/dictionary/english/reality (Accessed: 6 December 2023).
18. Wigner, E (1960). _The unreasonable effectiveness of mathematics in the natural sciences_. Available at: https://www.maths.ed.ac.uk/~v1ranick/papers/wigner.pdf (Accessed: 6 December 2023).
19. Alpha (2019). _Alpha Film Series // Episode 02 // Who is Jesus_ Available at: https://www.youtube.com/watch?v=HtTnSMNtE44 (Accessed: 6 December 2023).
20. Bible Project (2019). _Gospel_. Available at: https://www.youtube.com/watch?v=HT41M013X3A (Accessed: 6 December 2023).
21. drcraigvideos (2019). _Did Jesus Rise from the Dead? - Part One: The Facts_. Available at: https://www.youtube.com/watch?v=4qhQRMhUK1o (Accessed: 6 December 2023).
22. Got Questions Ministries (no date - c). _Is the Bible truly God's Word?_. Available at: https://www.gotquestions.org/Bible-God-Word.html (Accessed: 7 December 2023).
23. drcraigvideos (2023). _How Do We Determine Which Books Are Inspired?_. Available at: https://www.youtube.com/watch?v=2xRCQfVjeqg (Accessed: 7 December 2023).
24. Blomberg, C. and Dykes, J. N. (no date). _Jesus's View of the Old Testament_. Available at: https://www.thegospelcoalition.org/essay/jesuss-view-old-testament/ (Accessed: 7 December 2023).
25. Stewart, D. (no date). _What was Jesus' View of the Old Testament?_. Available at: https://www.blueletterbible.org/Comm/stewart_don/faq/bible-authoritative-word/question17-jesus-view-of-the-old-testament.cfm (Accessed: 7 December 2023).
26. CrossExamined.org (2021). _What Did Jesus Think About the Old Testament?_. Available at: https://crossexamined.org/what-did-jesus-think-about-the-old-testament/ (Accessed: 7 December 2023).
27. Bethinking (no date). _How did Jesus view the Old Testament?_. Available at: https://www.bethinking.org/bible/q-how-did-jesus-view-the-old-testament (Accessed: 7 December 2023).
28. Biblehub.com (no date) _Interlinear Bible_. Available at: https://biblehub.com/interlinear/ (Accessed: 8 December 2023).
29. Got Questions Ministries (no date - d). _Was the Apostle Paul actually a false prophet?_. Available at: https://www.gotquestions.org/apostle-Paul-false-prophet.html (Accessred: 8 December 2023).
30. Petruzello, M. (no date). _St. Paul's Contributions to the New Testament_. Available at: https://www.britannica.com/list/st-pauls-contributions-to-the-new-testament (Accessed: 8 December 2023).
31. Just, F. (2012). _The Deutero-Pauline Letters_. Available at: https://catholic-resources.org/Bible/Paul-Disputed.htm (Accessed: 8 December 2023).
32. Encyclopaedia Britannica (2020a). _Letters of John_. Available at: https://www.britannica.com/topic/letters-of-John (Accessed: 8 December 2023).
33. Encyclopaedia Britannica (2023a). _Letter to the Hebrews_. Available at: https://www.britannica.com/topic/Letter-to-the-Hebrews (Accessed: 8 December 2023).
34. Encyclopaedia Britannica (2023b). _Letter of James_. Available at: https://www.britannica.com/topic/Letter-of-James (Accessed: 8 December 2023).
35. Encyclopaedia Britannica (2020b). _Letter of Jude_. Available at: https://www.britannica.com/topic/Letter-of-Jude (Accessed: 8 December 2023).
36. Encyclopaedia Britannica (2023c). _Revelation to John_. Available at: https://www.britannica.com/topic/Revelation-to-John (Accessed: 8 December 2023).
37. Piercey, W. D., Fralick, P. C, and Scarborough, H. (2023). _hospital_. Available at: https://www.britannica.com/science/hospital (Accessed: 8 December 2023).
38. Encyclopaedia Britannica (2023). _university_. Available at: https://www.britannica.com/topic/university (Accessed: 8 December 2023).
39. Encyclopaedia Britannica (no date). _Church and education_. Available at: https://www.britannica.com/topic/Christianity/Church-and-education (Accessed: 14 December 2023).
40. Smith, J. M. P. (no date). _The Church and Education_. Available at: https://www.journals.uchicago.edu/doi/pdfplus/10.1086/480407 (Accessed: 14 December 2023).
41. Wikipedia (no date). _Montesquieu_. Available at: https://en.wikipedia.org/wiki/Montesquieu (Accessed: 14 December 2023).
42. Osler, M. J., Spencer, J. B., and Brush, S. G. (2019). _Scientific Revolution_. Available at: https://www.britannica.com/science/Scientific-Revolution (Accessed: 14 December 2023).
43. Encyclopaedia Britannica (no date). _Church and the individual_. Available at: https://www.britannica.com/topic/Christianity/Church-and-the-individual (Accessed: 14 December 2023).
