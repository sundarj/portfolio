# Azure (AZ-900) flashcards
A deck of flashcards I made in order to learn Azure and prepare for the AZ-900
exam: [AZ-900.mochi](/files/AZ-900.mochi). It is in the [Mochi](https://mochi.cards) format.

Spaced repetition is a research-backed learning technique [1, 2, 3]. It is
great for learning languages and other [declarative knowledge](https://en.wikipedia.org/wiki/Declarative_knowledge).
Mochi is an app that enables you to create decks of flashcards, which it gives
you spaced repetition tests (reviews) on periodically. During a review, you are
asked whether you could remember the card's answer, and if you could there is a
longer delay before the next review. If you couldn't there's a shorter delay.
Thus you are tested on cards you need to learn more frequently than cards you
know. This helps the cards to get into your long-term memory.

## References
1. [Why we can’t remember what we learn and what to do about it](https://interactive.wharton.upenn.edu/learning-insights/why-we-cant-remember-what-we-learn-and-what-to-do-about-it/).
2. [How to Remember Anything Forever-ish](https://ncase.me/remember/).
3. [What Works, What Doesn't](/files/what_works_what_doesnt.pdf).