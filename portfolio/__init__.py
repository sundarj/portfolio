from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

# from commonplace.two import commonplace

app = FastAPI()
# app.mount("/", commonplace.app)
app.mount("/", StaticFiles(directory="portfolio/static", html=True))
# app.host("commonplace.towardswisdom.dev", commonplace.app)
